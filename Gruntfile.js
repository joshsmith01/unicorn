module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    sass: {
      options: {
        includePaths: ['bower_components/foundation/scss']
      },
      dist: {
        options: {
          outputStyle: 'compressed',
          sourceMap: true,
        },
        files: {
          'css/app.css': 'scss/app.scss'
        }
      }
    },

    watch: {
      grunt: {
        options: {
          reload: true
        },
        files: ['Gruntfile.js']
      },

      sass: {
        files: 'scss/**/*.scss',
        tasks: ['sass']
      }
    },
    
    browserSync: {
    bsFiles: {
      src : 'assets/css/*.css'
    },
    options: {
      server: {
        baseDir: "./"
      }
    }
  },
  
  // Remove unused CSS across multiple files and ignore specific selectors
  uncss: {
    dist: {
      options: {
        ignore: ['#added_at_runtime', '.created_by_jQuery']
      },
      files: {
        'dist/css/tidy.css': ['index.html', 'index.php']
      }
    }
  }
  });

  grunt.loadNpmTasks('grunt-uncss');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');

  grunt.registerTask('build', ['sass']);
  grunt.registerTask('default', ['browserSync','watch']);
}
