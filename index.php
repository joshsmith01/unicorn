<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Unicorn - PSD to HTML</title>
    <link rel="stylesheet" href="css/app.css" />
<!--     <link rel="stylesheet" href="dist/css/tidy.css" /> -->
    <script src="bower_components/modernizr/modernizr.js"></script>
  </head>
  <body>
    <header data-interchange="[img/jpg/header-med-bg.jpg, (default)], [img/jpg/header-default-bg.jpg, (large)]">  
      <div class="contain-to-grid">
        <nav class="top-bar" data-topbar role="navigation">
          <ul class="title-area">
            <li class="name">
              <h1><a href="#">Unicorn</a></h1>
            </li>
             <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
          </ul>
      
          <section class="top-bar-section">
            <!-- Right Nav Section -->
            <ul class="right">
              <li class="active"><a href="#">Home</a></li>
              <li class="active"><a href="#">About</a></li>
              <li class="active"><a href="#">Stories</a></li>
              <li class="active"><a href="#">Hello</a></li>
            </ul>
          </section>
        </nav>
      </div><!--  END contain-to-grid -->
      <div class="header-intro">
        <div class="row">
          <div class="small-12 large-6 columns large-push-6 text-right">
            <h2>Storyteller</h2>
            <hr />
            <p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections.</p>
            <div class="border-button">read the rest</div>
          </div>
        </div>
      </div>
    </header>
    <section class="intro">
      <div class="row">
        <div class="small-12 columns text-center">
          <img src="img/png/broccoli.png" alt="broccoli" width="32" height="32">
          <h3>Who & Why</h3>
          <p>The gentlemen who rented the room would sometimes take their evening meal at home in the living room that was used by everyone, and so the door to this room was often kept closed in the evening. But Gregor found it easy to give up having the door open, he had, after all, often failed to make use of it when it was open and, without the family having noticed it, lain in his room in its darkest corner. One time, though, the charwoman left the door.</p>
        </div>
      </div>
    </section>
    <section class="services">
      <div class="row">
        <div class="small-12 medium-4 columns ">
          <i class="clock"></i>
          <h4>A Wow Feature</h4>
          <p>The gentlemen who rented the room would sometimes take their evening meal at home in the living.</p>
        </div>
        <div class="small-12 medium-4 columns">
          <i class="vector"></i>
          <h4>A Beautiful Feature</h4>
          <p>The gentlemen who rented the room would sometimes take their evening meal at home in the living.</p>
        </div>
        <div class="small-12 medium-4 columns">
          <i class="landworking"></i>
          <h4>An Amazing Feature</h4>
          <p>The gentlemen who rented the room would sometimes take their evening meal at home in the living.</p>
        </div>
      </div>
    </section>
    <section class="featured-articles">
      <div class="row">
        <div class="small-12 columns text-center">
          <h3>Featured Articles</h3>
          <p>The gentlemen who rented the room would sometimes take their evening meal at home in the living room that was used by everyone, and so the door to this room was often kept closed in the evening.</p>
          <div class="row" data-equalizer>
            <div class="small-12 medium-7 large-4 medium-centered large-uncentered columns featured-article text-left" data-equalizer-watch>
              <hr class="hr-accent-secondary small-1 columns small-centered"/>
              <div class="row padding">
                <h5 class="small-12 columns">Comic</h5>
                <h4 class="small-12 columns centered-for-small medium-text-left">A million ways to die in the wild wild west</h4>
                <img class="small-4 columns" src="img/png/user-headshots/user-03.png" alt="user-03" width="60" height="60">
                <div class="small-8 columns text-left">
                  <p class="article-author">Jimmy Jefferson</p>
                  <p class="article-date">21/08/2015</p>
                </div>
              </div>
            </div>
            <div class="small-12 medium-7 large-4 medium-centered large-uncentered  columns featured-article text-left" data-equalizer-watch>
              <hr class="hr-accent-secondary small-1 columns small-centered"/>
              <div class="row padding">
                <h5 class="small-12 columns">Lifestyle</h5>
                <h4 class="small-10 columns centered-for-small medium-text-left">The most comfortable chair comes with no backpain</h4>
                <img class="small-4 columns" src="img/png/user-headshots/user-01.png" alt="user-01" width="60" height="60">
                <div class="small-8 columns text-left">
                  <p class="article-author">Gail Gutierrez</p>
                  <p class="article-date">21/08/2015</p>
                </div>
              </div>
            </div>
            <div class="small-12 medium-7 large-4 medium-centered large-uncentered columns featured-article text-left" data-equalizer-watch>
              <hr class="hr-accent-secondary small-1 columns small-centered"/>
              <div class="row padding">
                <h5 class="small-12 columns">Travel</h5>  
                <h4 class="small-10 columns centered-for-small medium-text-left">journey to the center of the earth with jefferson </h4>
                <img class="small-4 columns" src="img/png/user-headshots/user-02.png" alt="user-02" width="60" height="60">
                <div class="small-8 columns text-left">
                  <p class="article-author">Mason Johnston</p>
                  <p class="article-date">21/08/2015</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="clients">
      <div class="row">
        <div class="small-12 columns text-center">
          <h3>Clients</h3>
          <div class="row">
            <div class="small-4 large-2 columns client-icon"><i><?php echo file_get_contents("img/svg/shield.svg");?></i></div>
            <div class="small-4 large-2 columns client-icon"><i><?php echo file_get_contents("img/svg/cube.svg");?></i></div>
            <div class="small-4 large-2 columns client-icon"><i><?php echo file_get_contents("img/svg/arrows.svg");?></i></div>
            <div class="small-4 large-2 columns client-icon"><i><?php echo file_get_contents("img/svg/computer.svg");?></i></div>
            <div class="small-4 large-2 columns client-icon"><i><?php echo file_get_contents("img/svg/atom.svg");?></i></div>
            <div class="small-4 large-2 columns client-icon"><i><?php echo file_get_contents("img/svg/glasses.svg");?></i></div>
          </div>
        </div>
      </div>
    </section>
    <section class="portfolio">
      <div class="row">
        <div class="small-12 columns text-center">
          <h3>  Portfolio & Screenshots</h3>
          <p>The gentlemen who rented the room would sometimes take their evening meal at home in the living room that was used by everyone, and so the door to this room was often kept closed in the evening.</p>
          <hr />
          <div class="small-12 medium-6 medium-push-3 centered">
            <div class="row categories">
              <div class="small-6 medium-2 columns centered-for-small-up">
                <p>Art</p>
              </div>
              <div class="small-6 medium-2 columns centered-for-small-up">
                <p>Mystery</p>
              </div>
              <div class="small-6 medium-2 columns centered-for-small-up">
                <p>Travel</p>
              </div>
              <div class="small-6 medium-2 columns centered-for-small-up">
                <p>Illusion</p>
              </div>
              <div class="small-6 medium-2 columns centered-for-small-up">
                <p>Dance</p>
              </div>
              <div class="small-6 medium-2 columns end centered-for-small-up">
                <p>Paintings</p>
              </div>
            </div><!--  END categories -->
          </div>
        </div>
        <div class="row text-center">
          <div class="small-12 medium-4 columns nopadding"><a href="#"><img src="img/png/portfolio-04.png" alt="portfolio-04" width="438" height="359"></a></div>
          <div class="small-12 medium-4 columns nopadding"><a href="#"><img src="img/png/portfolio-01.png" alt="portfolio-01" width="440" height="359"></a></div>
          <div class="small-12 medium-4 columns nopadding"><a href="#"><img src="img/png/portfolio-02.png" alt="portfolio-02" width="440" height="359"></a></div>
        </div>
      </div>
    </section>
    <section class="quote">
      <div class="row">
        <div class="small-12 medium-8 small-centered columns text-center">
          <i class="fa fa-quote-right"></i>
          <div class="blockquote">Happiness cannot be traveled to, owned, earned, or worn. It is the spiritual experience of living every minute with love, grace & gratitude.</div>
        <div class="cite">Denis Waitley</div></div>
      </div>
    </section>
    <section class="team-members">
      <div class="row">
        <div class="small-12 columns text-center">
          <h3>Team Members</h3>
          <p class="small-12 medium-8 columns text-center small-centered">The gentlemen who rented the room would sometimes take their evening meal at home in the living room that was used by everyone, and so the door to this room was often kept closed in the evening.</p>
        </div>
      </div>
      <div class="row">
        <div class="small-12 medium-6 large-3 columns team-member text-center">
          <?php echo file_get_contents("img/svg/james-philly.svg");?>
          <p class="team-member-name">James Philly</p>
          <p class="team-member-role">Lead Developer</p>
        </div>
        <div class="small-12 medium-6 large-3 columns team-member text-center">
          <?php echo file_get_contents("img/svg/cactus-jack.svg");?>
          <p class="team-member-name">Cactus Jack</p>
          <p class="team-member-role">3D Model Designer</p>
        </div>
        <div class="small-12 medium-6 large-3 columns team-member text-center">
          <?php echo file_get_contents("img/svg/jack-sparrow.svg");?>
          <p class="team-member-name">Jack Sparrow</p>
          <p class="team-member-role">Master of All Trade</p>
        </div>
        <div class="small-12 medium-6 large-3 columns team-member text-center">
          <?php echo file_get_contents("img/svg/yaga-squarehead.svg");?>
          <p class="team-member-name">Yaga Squarehead</p>
          <p class="team-member-role">Manages Money</p>
        </div>
      </div><!--  END row -->
    </section>
    <section class="facts">
      <div class="row">
        <div class="small-12 columns text-center">
          <h3>Facts & Numbers</h3>
        </div>
      </div>
      <div class="row">
        <div class="small-12 medium-10  large-8 columns small-centered text-center data">
          <div class="row">
            <div class="small-6 columns">
              <h4>87</h4>
              <hr />
              <p>happy people</p>
            </div>
            <div class="small-6 columns">
              <h4>142</h4>
              <hr />
              <p>succesful people</p>
            </div>
          </div>
          <div class="row">
            <div class="small-6 columns">
              <h4>110</h4>
              <hr />
              <p>thrilled people</p>
            </div>
            <div class="small-6 columns">
              <h4>90</h4>
              <hr class="hr-accent-primary" />
              <p>elated people</p>
            </div>
          </div>
        </div>
        <hr class="hr-accent-secondary small-1 columns small-centered"/>
      </div>
    </section>
    <section class="blog">
      <div class="row">
        <div class="small-12 medium-6 columns text-center excerpt">
          <h4>Travel/21st March, 2014</h4>
          <h3>A New Product is Coming</h3>
          <p>The office assistant was the boss's man, spineless, and with no understanding. What about if he reported sick? But that would be extremely strained and suspicious as in fifteen years of service Gregor had never.</p>
        </div>
        <div class="small-12 medium-6 columns excerpt blog-01"><a href="#"><img src="img/png/blog-01.png" alt="blog-01" width="571" height="381"></a></div>
      </div>
      <div class="row">
        <div class="small-12 medium-6 medium-push-6 columns text-center excerpt">
          <h4>Gadget / 3rd March, 2014</h4>
          <h3>Jimmy’s New Xbox Controller</h3>
          <p>The office assistant was the boss's man, spineless, and with no understanding. What about if he reported sick? But that would be extremely strained and suspicious as in fifteen years of service Gregor had never.</p>
        </div>
        <div class="small-12 medium-6 medium-pull-6 columns excerpt"><a href="#"><img src="img/png/blog-02.png" alt="blog-02" width="570" height="380"></a></div>
      </div>
      <div class="row">
        <div class="small-12 medium-6 columns text-center excerpt">
          <h4>Travel/21st March, 2014</h4>
          <h3>A New Product is Coming</h3>
          <p>The office assistant was the boss's man, spineless, and with no understanding. What about if he reported sick? But that would be extremely strained and suspicious as in fifteen years of service Gregor had never.</p>
        </div>
        <div class="small-12 medium-6 columns excerpt"><a href="#"><img src="img/png/blog-03.png" alt="blog-03" width="569" height="379"></a></div>
      </div>
    </section>
    <section class="old-articles">
      <div class="row">
        <div class="small-12 columns">
          <div class="row">
            <div class="small-12 medium-4 columns old-article">
              <h4>Another Day in Paradise</h4>
              <p>Travel/ 21st March, 2014</p>
            </div>
            <div class="small-12 medium-4 columns old-article">
              <h4>Jimmy's New Book on Aliens</h4>
              <p>Travel/ 21st March, 2014</p>
            </div>
            <div class="small-12 medium-4 columns old-article">
              <h4>Sarah's New House</h4>
              <p>Travel/ 21st March, 2014</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="tweets">
      <div class="row">
        <div class="small-7 small-centered columns text-center">
          <i class="fa fa-twitter animated jello"></i>
          <a href="#"><img src="img/png/user-headshots/user-01.png" alt="user-02" width="60" height="60"></a>
          <p>Happiness cannot be traveled to, owned, earned, or worn. It is the spiritual experience of living every minute with love, grace & gratitude.</p>
          <a href="#" class="tweet-link">@loveless</a>
        </div>
      </div>
    </section>
    <footer>
      <div class="row">
        <div class="small-12 medium-5 columns">
          <h3>Unicorn</h3>
          <p class="contact">One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his</p>
          <form>
            <div class="row">
              <div class="large-6 columns">
                <label class="">
                  <input type="text" class="error" name="email" placeholder="EMAIL" />
                </label>
              </div>
              <div class="large-6 columns error">
                <label>
                  <input type="text" name="subject" placeholder="SUBJECT" />
                </label>
              </div>
            </div>
            <textarea class="" name="message" placeholder="MESSAGE"></textarea>
            <button class="footer-form-button">SEND</button>
          </form>
        </div>
        <div class="small-12 medium-6 columns">
          <div class="row">
            <div class="small-4 columns">
              <p class="category title">Links</p>
              <ul>
                <li><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Services</a></li>
                <li><a href="#">Menu</a></li>
                <li><a href="#">Restaurants</a></li>
                <li><a href="#">Work Hours</a></li>
                <li><a href="#">Call Hours</a></li>
              </ul>
            </div>
            <div class="small-4 columns">
              <p class="category title">Friends</p>
              <ul>
                <li><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Services</a></li>
                <li><a href="#">Menu</a></li>
                <li><a href="#">Restaurants</a></li>
              </ul>
            </div>
            <div class="small-4 columns">
              <p class="category title">Social</p>
              <ul>
                <li><a href="#">Facebook</a></li>
                <li><a href="#">Twitter</a></li>
                <li><a href="#">GitHub</a></li>
                <li><a href="#">Pinterest</a></li>
                <li><a href="#">Google Plus</a></li>
                <li><a href="#">Dribbble</a></li>
                <li><a href="#">Flickr</a></li>
                <li><a href="#">Others</a></li>
              </ul>                      
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="small-12 medium-6 columns">
          <p> One morning, when Gregor Samsa woke from troubled dreams, he found.</p>
          <p class="colophon">&copy; 2015 Your Awesome Company</p>
        </div>
        <div class="small-12 medium-6 columns ">
          <ul class="inline-list right">
            <li><a href="#">Terms</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Contact</a></li>
            <li><a href="#">Jobs</a></li>
          </ul>
        </div>
      </div>
    </footer>


    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/app.js"></script>

  </body>
</html>
